using UnityEngine;
using UnityEngine.UI;

public class SettingsButtonController : MonoBehaviour
{
    private byte numSprite;
    private Image image;
    [SerializeField] private Sprite[] buttonSprites;

    private void Start()
    {
        image = GetComponent<Image>();
        if (PlayerPrefs.GetString(this.name) == "on")
            numSprite = 0;
        else
            numSprite = 1;
            
        image.sprite = buttonSprites[numSprite];
    }

    public void SwitchSetting()
    {
        if (numSprite == 0)
        {
            numSprite = 1;
            PlayerPrefs.SetString(this.name, "off");
        }
        else
        {
            numSprite = 0;
            PlayerPrefs.SetString(this.name, "on");
        }
        image.sprite = buttonSprites[numSprite];
    }

    public void MusicSwitch(AudioSource musicSource)
    {
        musicSource.enabled = !musicSource.enabled;
    }
}