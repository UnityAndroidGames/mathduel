using UnityEngine.UI;
using UnityEngine;
public class VariantButtonsController : MonoBehaviour
{
    private Text textAnswer;
    private CalcManager calcManager;
    private void Start()
    {
        textAnswer = GetComponentInChildren<Text>();
        calcManager = transform.parent.gameObject.transform.parent.gameObject.GetComponent<CalcManager>();
    }
    public void SendAnswer()
    {
        string answer = textAnswer.text;
        calcManager.CheckResult(answer);
    }
}