using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsButtonsAnimationController : MonoBehaviour
{
    private List<Transform> settingButtons = new List<Transform>();
    private bool isOpen = false;

    private void Start()
    {
        settingButtons.AddRange(GetComponentsInChildren<Transform>(true));
        settingButtons.RemoveAt(0);
    }
    public void OpenSettings()
    {
        if (isOpen == false)
        {
            foreach (Transform button in settingButtons)
            {
                button.GetComponent<Image>().DOFade(1, 1);
                button.GetComponent<Image>().raycastTarget = true;
            }
            settingButtons[0].DOLocalMoveY(250, 1);
            settingButtons[1].DOLocalMoveX(-250, 1);
            settingButtons[2].DOLocalMoveX(250, 1);
            isOpen = true;
        }
        else
        {
            settingButtons[0].DOLocalMoveY(0, 1);
            settingButtons[1].DOLocalMoveX(0, 1);
            settingButtons[2].DOLocalMoveX(0, 1);
            foreach (Transform button in settingButtons)
            {
                button.GetComponent<Image>().DOFade(0, 1);
                button.GetComponent<Image>().raycastTarget = false;
            }
            isOpen = false;
        }
    }

    public void OnDestroy()
    {
        DOTween.KillAll();
    }
}
