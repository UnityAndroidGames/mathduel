using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchShopButtonController : MonoBehaviour
{
    private GameObject charactersShop, backgroundsShop;
    [SerializeField] LocalizationManager localizationManager;

    private void OnEnable()
    {
        SwitchLanguage();
    }
    private void Start()
    {
        charactersShop = transform.GetChild(2).gameObject;
        backgroundsShop = transform.GetChild(3).gameObject; 
    }
    public void SwitchShop()
    {
        charactersShop.SetActive(!charactersShop.activeSelf);
        backgroundsShop.SetActive(!backgroundsShop.activeSelf);
        SwitchLanguage();
    }

    private void SwitchLanguage()
    {
        if (PlayerPrefs.GetString("Language") == "on")
            localizationManager.SetLanguage(0);
        else
            localizationManager.SetLanguage(1);
    }
}
