using UnityEngine;
using UnityEngine.UI;

public class LocalizationButtonController : MonoBehaviour
{
    [SerializeField] LocalizationManager localizationManager;

    public void SwitchLanguage()
    {
        if (PlayerPrefs.GetString("Language") == "on")
            localizationManager.SetLanguage(0);
        else
            localizationManager.SetLanguage(1);
    }
}
