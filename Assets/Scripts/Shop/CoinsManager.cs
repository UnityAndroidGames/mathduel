using UnityEngine;
using UnityEngine.UI;

public class CoinsManager : MonoBehaviour
{
    [SerializeField] private Text countCoinsText;
    private int countCoins;
    private void Start()
    {
        ShowCoins();
    }
    public void AddCoins(int countCoins)
    {
        int coins = PlayerPrefs.GetInt("Coins");
        coins += countCoins;
        PlayerPrefs.SetInt("Coins", coins);
        ShowCoins();
    }

    public void ShowCoins()
    {
        if (countCoinsText != null)
        {
            countCoins = PlayerPrefs.GetInt("Coins");
            countCoinsText.text = countCoins.ToString();
        }
    }
}