using UnityEngine;

public class BackgroundsManager : MonoBehaviour
{
    [SerializeField] private BundleBackgrounds backgroundsBundle;
    [SerializeField] private SpriteRenderer background;
    private GameObject backgroundS;
    private void Start()
    {
        SetBackground(PlayerPrefs.GetString("CurrentBackground"));
    }

    public void SetBackground(string currentBackground)
    {
        for (int i = 0; i < backgroundsBundle.Backgrounds.Length; i++)
            if (backgroundsBundle.Backgrounds[i].name == currentBackground)
            {
                background.sprite = backgroundsBundle.Backgrounds[i];
            }
    }
}