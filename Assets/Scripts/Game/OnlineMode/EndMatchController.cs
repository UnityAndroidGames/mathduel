using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class EndMatchController : MonoBehaviour
{
    [SerializeField] private GameObject endMatchScreen;
    [SerializeField] private LocalizedText localizedText;
    [SerializeField] private GameObject characters;
    private AnimationController playerAnimation, enemyAnimation;
    private Slider myHP;
    private CoinsManager coinsManager;
    private SpriteRenderer coinSprite;

    private void Start()
    {
        myHP = transform.GetChild(0).GetComponent<Slider>();
        coinsManager = endMatchScreen.GetComponent<CoinsManager>();
        coinSprite = endMatchScreen.transform.GetChild(5).GetComponent<SpriteRenderer>();
    }
    public void ShowScreen()
    {
        for (int i = 0; i < (transform.childCount - 2); i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        endMatchScreen.SetActive(true);
        endMatchScreen.GetComponent<StatisticsManager>().ShowStatistics();
        playerAnimation = characters.transform.GetChild(0).gameObject.GetComponentInChildren<AnimationController>();
        enemyAnimation = characters.transform.GetChild(1).gameObject.GetComponentInChildren<AnimationController>();
        StartCoroutine("ShowMatchResult");
    }

    private IEnumerator ShowMatchResult()
    {
        yield return new WaitWhile(() => !endMatchScreen.activeSelf);
        if (myHP.value < 1f)
        {
            playerAnimation.DefeatAnimation();
            enemyAnimation.VictoryAnimation();
            localizedText.Localize("LoseText");
        }
        else
        {
            playerAnimation.VictoryAnimation();
            enemyAnimation.DefeatAnimation();
            localizedText.Localize("WinText");
            coinsManager.AddCoins(1);
            PlayerPrefs.SetInt("WinCount", PlayerPrefs.GetInt("WinCount") + 1);
            coinSprite.enabled = true;
        }
    }
}
