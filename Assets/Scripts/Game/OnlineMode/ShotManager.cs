using System.Collections;
using UnityEngine;

public class ShotManager : MonoBehaviour
{
    [SerializeField] private GameObject damageInfoPlayerPrefab, damageInfoEnemyPrefab;
    [SerializeField] private StatisticsManager statisticsManager;
    [SerializeField] private HPController playerHPController, enemyHPController;
    private HPController currentHPController;
    private GameObject damageInfo, previousDamageInfo;
    private AnimationController attacker, attacked;
    private TimerController timerController;
    public DataExchangeManager MyDataExchanger { get; set; }
    public AnimationController playerAnimation { get; set; }
    public AnimationController enemyAnimation { get; set; }

    private void Start()
    {
        timerController = GetComponent<TimerController>();
        timerController.enabled = true;
    }

    public void Shot(int stateShot, bool isIncoming)
    {
        statisticsManager.gettingValues(isIncoming, stateShot);
        if (!isIncoming)
        {
            currentHPController = enemyHPController;
            MyDataExchanger.OnShot(stateShot);
            timerController.ShotCount++;
            attacker = playerAnimation;
            attacked = enemyAnimation;
        }
        else
        {
            timerController.ShotCount++;
            currentHPController = playerHPController;
            attacker = enemyAnimation;
            attacked = playerAnimation;
        }
        switch (stateShot)
        {
            case 0:
                StartCoroutine(OnCompleteAttackAnimation("Miss", stateShot));
                break;
            case 1:
                StartCoroutine(OnCompleteAttackAnimation("Damaged", stateShot));
                currentHPController.ChangeHPValue();
                break;
        }
        attacker.AttackAnimation();
    }

    private IEnumerator OnCompleteAttackAnimation(string text, int stateShot)
    {
        yield return new WaitForSeconds(attacker.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length / 25);
        if (previousDamageInfo != null)
            Destroy(previousDamageInfo);
        if (currentHPController == playerHPController)
            damageInfo = Instantiate(damageInfoPlayerPrefab, transform.GetChild(0));
        else
            damageInfo = Instantiate(damageInfoEnemyPrefab, transform.GetChild(1));
        damageInfo.GetComponent<ShowDamageInfo>().text = text;
        previousDamageInfo = damageInfo;
        if (stateShot > 0)
            attacked.AttackedAnimation();
    }
 }