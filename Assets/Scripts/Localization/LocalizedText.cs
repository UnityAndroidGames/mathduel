using System;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    [SerializeField] private string keyIn;
    private Text text;
    private string key;

    private void Start()
    {
        Localize(keyIn);
    }

    private void OnLanguageChange()
    {
        Localize();
    }

    private void Init()
    {
        text = GetComponent<Text>();
        key = text.text;
    }
    public void Localize(string newKey = null)
    {
        if (text == null)
            Init();
        if (newKey != null)
            key = newKey;
        text.text = LocalizationManager.GetTranslate(key);
    }

    private void OnEnable()
    {
        LocalizationManager.OnLanguageChange += OnLanguageChange;
    }
    private void OnDisable()
    {
        LocalizationManager.OnLanguageChange -= OnLanguageChange;
    }
}
